# Activity

# [2]
name = "Camille"
age = 26
occupation = "Software Engineer"
movie = "One More Chance"
rating = 99.6

print(f"I am {name} and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")


# [3]
num1, num2, num3 = 10, 15, 20
print(num1 * num2)  # 150
print(num1 < num3)  # True
print(num3 + num2)  # 35



